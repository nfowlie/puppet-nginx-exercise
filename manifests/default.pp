class nginx_exercise {
  # Silence the annoying deprecation warning.
  Package {
    allow_virtual => true,
  }

  class { 'nginx':
    root_group => 'www-data',    
  }

  # TODO: It would be better to reconfigure selinux to allow port 8000
  # instead of turning it off completely.
  class { 'selinux':
    mode => 'disabled',
  }

  package { 'git':
    ensure => latest,
  }

  group { 'www-data':
    ensure               => present,
    attribute_membership => minimum,
  }

  user { 'nginx':
    ensure  => present,
    groups  => ['www-data'],
    require => Group['www-data'],
  }

  file { '/var/www':
    ensure  => directory,
    owner   => 'root',
    group   => 'www-data',
    mode    => '0644',
    links   => 'follow',
    recurse => true,  
  }

  file { '/var/www/nginx-example':
    ensure  => directory,
    recurse => true,
    ignore  => '.git',
    source  => '/var/tmp/puppet/files/exercise-webpage',
    owner   => 'root',
    group   => 'www-data',
    mode    => '0644',
    links   => 'follow',
  }

  nginx::resource::vhost { 'nginx-example':
    ensure      => present,
    listen_port => 8000,
    server_name => ['_'],
    www_root    => '/var/www/nginx-example',
    access_log  => '/var/log/nginx/nginx-example_access.log',
    error_log   => '/var/log/nginx/nginx-example_error.log',
    index_files => ['index', 'index.htm', 'index.html'],
  }

  vcsrepo { '/var/tmp/puppet/files/exercise-webpage':
    ensure   => latest,
    provider => git,
    source   => 'https://github.com/puppetlabs/exercise-webpage.git',
    revision => 'master'    
  }
}

include nginx_exercise
