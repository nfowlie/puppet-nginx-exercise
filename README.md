# nginx_exercise

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with nginx_exercise](#setup)
    * [What nginx_exercise affects](#what-nginx_exercise-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with nginx_exercise](#beginning-with-nginx_exercise)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Solution to the puppetlabs nginx exercise.

## Module Description

Sample solution to the puppetlabs nginx exercise. To execute, call puppet apply manifests/init.pp inside the module directory.

## Setup

### What nginx_exercise affects

* The latest version of nginx will be installed if it is not present.
* The latest version of git will be installed if it is not present.
* The contents of /var/www/nginx_exercise will be over-written. Any changes made to this directory will be lost.
* A new virtualhost called nginx-exercise will be added to nginx and made available.
* The default nginx virtualhost will be disabled.
* SELinux will be disabled.

### Setup Requirements **OPTIONAL**

Ensure you have run the vagrant bootstrap script prior to applying this module. The following dependencies are assumed to already be available on the system:

* Git
* puppetlabs/git module
* jfryman/nginx module
* jfryman/selinux module
* nginx is in the list of available yum packages.

### Beginning with nginx_exercise

puppet apply manifests/init.pp

## Usage

puppet apply manifests/init.pp

## Reference


## Limitations

## Development


## Release Notes/Contributors/Etc

